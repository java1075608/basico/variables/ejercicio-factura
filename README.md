# Ejemplo básico de una factura.
### La tarea consiste en crear una nueva clase con el método main llamada DetalleDeFactura, se requiere desarrollar un programa que reciba datos de la factura utilizando la clase Scanner de la siguiente manera:
* Reciba el nombre de la factura o descripción, utilizar método `nextLine()` para obtener el nombre de la factura con espacios.
* Reciba 2 precios de productos de tipo `double`, debe utilizar el método `nextDouble()` para obtener precios con decimales `(,)`.
* Calcule el total, sume ambos precios y agregue un valor de impuesto del 19%.