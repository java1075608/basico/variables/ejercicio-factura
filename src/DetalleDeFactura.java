import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Jonathan Lizama
 */
public class DetalleDeFactura {
    final static double iva = 0.19;

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingrese el nombre de la Factura: ");
        String nombreFactura = validarNombreFactura(leer);
        System.out.println("Ingrese el precio del producto 1 ");
        double precio1 = validarPrecio(leer);
        System.out.println("Ingrese el precio del producto 2");
        double precio2 = validarPrecio(leer);

        double subTotal = (precio1 + precio2);
        double impuesto = (subTotal * iva);
        double total = subTotal + impuesto;

        System.out.println("---------------------------------------------------------");
        System.out.println("El nombre de la factura es: " + nombreFactura + "\n" + "Monto bruto: " + subTotal + "\n" + "Impuesto: " + impuesto + "\n" + "Total neto: " + total);
        System.out.println("---------------------------------------------------------");
    }


    /**
     *
     * @param leer entrada de dato por consola.
     * @return precio de un producto.
     */
    public static double validarPrecio(Scanner leer) {
        double precio = 0.0;
        try {
            do {
                precio = leer.nextDouble();
            } while (precio <= 0.0);
        } catch (InputMismatchException e) {
            System.out.println("Debe ingresar un precio valido!!!");
        }
        return precio;
    }

    /**
     *
     * @param leer entrada de dato por consola.
     * @return nombre de la factura.
     */
    public static String validarNombreFactura(Scanner leer){
        String nombre = "";
        try{
            do {
                nombre = leer.nextLine();
            } while (nombre.length() < 3);
        }catch(InputMismatchException e){
            System.out.println("Debe ingresar un nombre valido!!!");
        }
        return  nombre;
    }

}